const ColumnHeader = {
    backgroundColor: 'red', 
    padding: 10,
    color: 'white',
    fontSize: "22px",
    fontWeight: 'bold'
}

const ColumnTitle = {
    marginBottom: 8 ,
    paddingLeft: 10,
    color: 'black',
    fontSize: "16px"
}

const ColumnTitleBold = {
    marginBottom: 8 ,
    paddingLeft: 10,
    color: 'black',
    fontSize: "16px",
    fontWeight: 'bold'
}

const ColumnTitleBoldRed = {
    marginBottom: 8 ,
    paddingLeft: 10,
    color: 'red',
    fontSize: "16px",
    fontWeight: 'bold'
}

const ColumnTitleBoldWhite = {
    marginBottom: 8 ,
    paddingLeft: 10,
    color: 'white',
    fontSize: "18px",
    fontWeight: 'bold'
}

const TimlineTitle = {
  paddingBottom: 50,
  color: 'black',
  fontSize: "18px",
}

const SCGLogo = {
  height: 60,
  width: 110,
  marginLeft: 20,
  marginTop: 20
}

export default {
    ColumnHeader: ColumnHeader,
    ColumnTitle: ColumnTitle,
    ColumnTitleBold: ColumnTitleBold,
    ColumnTitleBoldRed: ColumnTitleBoldRed,
    ColumnTitleBoldWhite: ColumnTitleBoldWhite,
    TimlineTitle: TimlineTitle,
    SCGLogo: SCGLogo
};
  