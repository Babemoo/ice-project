import React, { Component } from  "react";
import { Row, Col, Divider, Timeline, Input  } from 'antd';

import {  CheckCircleTwoTone } from '@ant-design/icons';



import '../App.css';

// Import the styles
import styles from "../Styles";

//Create 'class component'
class Body extends Component{

    constructor(props){
        super(props);
    }

    render(){
       
        const {PINumber,OrderNumber,BookingNumber,ShippingAgentAddress,ShipAgent,ShipTo,ETDDate,ETADate,OrderStatus}=this.props;
        const { Search } = Input;


const onSearch = value => console.log(value);

        return (
            <div>
                <Divider orientation="left"></Divider>
                <Row justify="center" style = {{paddingBottom: 25}} >
                    <Col span={16}>
                        <Search
                            placeholder="Input PO Number"
                            allowClear
                            enterButton="Search"
                            size="large"
                            onSearch={onSearch}
                        />
                    </Col>
                </Row>
                    

                <Row justify="center">
                    <Col span={16}>
                        <p style={styles.ColumnHeader}> Shipment Tracking Details </p>
                    </Col>
                </Row>

                <Row justify="center">
                    <Col span={5}>
                        <p style={styles.ColumnTitleBold}> PI Number: </p>
                        <p style={styles.ColumnTitleBold}> Order Number: </p>
                        <p style={styles.ColumnTitle}> Booking Number: </p>
                        <p style={styles.ColumnTitle}> Shipping Agent Address: </p>
                    </Col>
                    <Col span={4}>
                        <p style={styles.ColumnTitleBold}>{PINumber}</p>
                        <p style={styles.ColumnTitleBoldRed}>{OrderNumber}</p>
                        <p style={styles.ColumnTitle}>{BookingNumber} </p>
                        <p style={styles.ColumnTitle}>{ShippingAgentAddress} </p>
                    </Col>
                    <Col span={7}>

                    </Col>
                </Row>

                <Row justify="center">
                    <Col span={5}>
                        <p style={styles.ColumnTitle}>Ship Agent: </p>
                        <p style={styles.ColumnTitle}>ETD: </p>
                    </Col>
                    <Col span={4}> 
                        <p style={styles.ColumnTitle}>{ShipAgent}</p>
                        <p style={styles.ColumnTitle}>{ETDDate}</p>
                    </Col>
                    <Col span={3}>
                        <p style={styles.ColumnTitle}> Ship To: </p>
                        <p style={styles.ColumnTitle}>ETA: </p>
                    </Col>
                    <Col span={4}>
                        <p style={styles.ColumnTitle}>{ShipTo}</p>
                        <p style={styles.ColumnTitle}>{ETADate}</p>
                    </Col>
                </Row>
                
                <Row justify="center">
                    <Col span={5}  style= {{backgroundColor: '#E5DFDF'}}> 
                        <p style={styles.ColumnTitleBold}> Order Status: </p>
                    </Col>
                    <Col span={4}  style= {{backgroundColor: '#E5DFDF'}}>  
                        <p style={styles.ColumnTitleBoldRed}>{OrderStatus}</p>
                    </Col>
                    <Col span={7} style= {{backgroundColor: '#E5DFDF'}}>

                    </Col>
                </Row>

                <Row justify="center">
                    <Col span={16}  style= {{backgroundColor: '#6E6868'}}> 
                        <p style={styles.ColumnTitleBoldWhite}> Shipping Summary: </p>
                    </Col>
                </Row>
                <Row justify="center" >
                <Col span={16}>
                    <Timeline mode="left" style = {{paddingTop: 50}}>

                        <Timeline.Item label="2015-09-01" style = {styles.TimlineTitle} dot={< CheckCircleTwoTone  className="icons-list" twoToneColor="#52c41a" style={{fontSize: 30 }}/>} >
                            Create a services site
                        </Timeline.Item>

                        <Timeline.Item label="2015-09-01 09:12:11" style = {styles.TimlineTitle} dot={< CheckCircleTwoTone  className="icons-list" twoToneColor="#52c41a" style={{fontSize: 30 }}/>} >
                            Solve initial network problems 
                        </Timeline.Item>

                        <Timeline.Item style = {styles.TimlineTitle} dot={< CheckCircleTwoTone  className="icons-list" twoToneColor="#52c41a" style={{fontSize: 30 }}/>} >
                            Technical testing
                        </Timeline.Item>

                        <Timeline.Item label="2015-09-01 09:12:11" style = {styles.TimlineTitle} dot={< CheckCircleTwoTone  className="icons-list" twoToneColor="#52c41a" style={{fontSize: 30 }}/>} >
                            Network problems being solved
                        </Timeline.Item>

                    </Timeline>
                    </Col>
                </Row>
            </div>
        )
    }
}
//declare variable and write function into it call 'functional component'
/*
const Body = (props) => {
    const {PINumber,OrderNumber,BookingNumber,ShipAgent,ShipTo,ETDDate,ETADate,OrderStatus}=props;

    return (
        <div>
            <p>PI Number: {PINumber}</p>
            <p>Order Number: {OrderNumber}</p>
            <p>Booking Number: {BookingNumber}</p>
            <p>Ship Agent: {ShipAgent}</p>
            <p>Ship To: {ShipTo}</p>
            <p>ETD: {ETDDate}</p>
            <p>ETA: {ETADate}</p>
            <p>Order Status: {OrderStatus}</p>

        </div>
    );
}
*/
export default Body;