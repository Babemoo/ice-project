import React, {Component} from 'react';
import Header from "./components/Header"
import Body from "./components/Body"
import Footer from "./components/Footer"

class App extends Component {
  render () {
    return (
       
      // JSX to render goes here...
      <div>
        
        <Header />
        <Body PINumber="1212312121" OrderNumber ="1212312121" BookingNumber ="ABCDE" ShippingAgentAddress="www.mearsk.com" ShipAgent="OOCL" ShipTo = "Tokyo, Japan" ETDDate = "May 10,2021" ETADate ="May 20,2021" OrderStatus="Delivered"/>
        <Footer />
      </div>
       
    );
  }
}

 export default App;
 
/*
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
*/
